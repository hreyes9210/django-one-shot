from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateListForm, CreateItemForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": todos,
    }
    return render(request, "todos/detail.html", context)


#instruction have not told us to use @login required
def todo_list_create(request):
    if request.method == "POST":
        form = CreateListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
#i confirmed this is not referencing your todo lsit detail 
# #function above i believe its referencing your url.py path
    else:
        form = CreateListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_list_edit(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateListForm(request.POST, instance=post)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateListForm(instance=post)
    context = {
        "object": post,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_item_edit(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = CreateItemForm(request.POST, instance=post)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
#if did list instead of new_item in 76 was not redirecting to the correct list
    else:
        form = CreateItemForm(instance=post)

    context = {
        "object": post,
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)


def delete_model_name(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")
    #name assigned on the url page for list page
    return render(request, "todos/delete.html")

 