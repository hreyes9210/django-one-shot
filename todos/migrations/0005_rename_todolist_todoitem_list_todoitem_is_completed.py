# Generated by Django 4.2.6 on 2023-10-30 19:48

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0004_remove_todoitem_is_completed"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="todolist",
            new_name="list",
        ),
        migrations.AddField(
            model_name="todoitem",
            name="is_completed",
            field=models.BooleanField(default=False),
        ),
    ]
