from django.urls import path
from todos.views import todo_list_list, todo_list_detail, todo_list_create
from todos.views import todo_list_edit, delete_model_name, todo_item_create
from todos.views import todo_item_edit


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"), 
    #*****this is registering the view.py with the path aka the url.py
    #lesson spcefically said to name it todo list list
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("<int:id>/edit/", todo_list_edit, name="todo_list_update"),
    path("items/<int:id>/edit/", todo_item_edit, name="todo_item_update"),
    path("<int:id>/delete/", delete_model_name, name="todo_list_delete")
]

    #the other url.py is registering the specific todo urls as one of the db urls options
    #other options include /admin, /account /create
    #none of the other options have todo in the name!!