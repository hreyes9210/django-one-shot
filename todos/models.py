from django.db import models

#models define the structure of how you want to store
#the data in your database
#remember, creates the tables to store dat a inthe background


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
        )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        "TodoList",
        related_name="items",
        on_delete=models.CASCADE,
    )